/* const replaceText = require('utils.js').replaceText; */

let precios = {};

const loadPrecios = () => {
    return fetch("prices.json")
        .then(data => data.json());
}

const updateView = () => {
    replaceValorTicket(precios.valorTicket);
    replaceDescuentoEstudiante(precios.descuentoEstudiante * 100);
    replaceDescuentoTrainee(precios.descuentoTrainee * 100);
    replaceDescuentoJunior(precios.descuentoJunior * 100);
}

const replaceValorTicket = (precio) => {
    replaceText("valor-ticket", precio);
}

const replaceDescuentoEstudiante = (precio) => {
    replaceText("descuento-estudiante", precio);
}

const replaceDescuentoTrainee = (precio) => {
    replaceText("descuento-trainee", precio);
}

const replaceDescuentoJunior = (precio) => {
    replaceText("descuento-junior", precio);
}

loadPrecios()
    .then(respPrecios => {
        precios = respPrecios;
        updateView();
    })
    .catch(error => console.error("Se produjo un error al obtener precios", JSON.stringify(error)));

document.addEventListener("DOMContentLoaded", loadPrecios, false);