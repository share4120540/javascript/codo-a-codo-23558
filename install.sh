#!/bin/bash

# Crea los directorios necesarios
mkdir -p public/assets

# Copia los archivos y directorios
cp -r assets public
cp style.css public
cp index.html public
cp tickets.html public
cp utils.js public
cp form-utils.js public
cp load-prices.js public
cp prices.json public
cp bootstrap-lux.css public